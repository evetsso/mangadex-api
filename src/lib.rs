mod constants;
mod cookie;
mod helpers;
pub mod types;
#[cfg(feature = "v2")]
pub mod v2;
pub mod web_scrape;

/// Base trait for defining MangaDex clients.
pub trait MangaDexClient {
    /// Return the HTTP client that makes HTTP requests.
    fn client(&self) -> &reqwest::Client;

    /// Return the base MangaDex URL.
    fn base_url(&self) -> &str {
        "https://mangadex.org"
    }

    /// Return the base MangaDex API URL.
    fn base_api_url(&self) -> &str {
        "https://api.mangadex.org"
    }

    /// Return the base MangaDex API URL
    fn api_url(&self) -> String;
}
