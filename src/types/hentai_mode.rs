use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, Hash, PartialEq, PartialOrd, Serialize)]
#[serde(from = "u8")]
#[repr(u8)]
pub enum HentaiMode {
    ShowAll = 1,
    ShowOnly = 2,
    #[serde(other)]
    Hide = 0,
}

impl From<u8> for HentaiMode {
    fn from(value: u8) -> Self {
        match value {
            1 => Self::ShowAll,
            2 => Self::ShowOnly,
            _ => Self::Hide,
        }
    }
}

impl From<u16> for HentaiMode {
    fn from(value: u16) -> Self {
        match value {
            1 => Self::ShowAll,
            2 => Self::ShowOnly,
            _ => Self::Hide,
        }
    }
}

impl From<u32> for HentaiMode {
    fn from(value: u32) -> Self {
        match value {
            1 => Self::ShowAll,
            2 => Self::ShowOnly,
            _ => Self::Hide,
        }
    }
}

impl From<u64> for HentaiMode {
    fn from(value: u64) -> Self {
        match value {
            1 => Self::ShowAll,
            2 => Self::ShowOnly,
            _ => Self::Hide,
        }
    }
}
