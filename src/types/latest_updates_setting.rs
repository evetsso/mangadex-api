use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, Hash, PartialEq, PartialOrd, Serialize)]
#[serde(from = "u8")]
#[repr(u8)]
pub enum LatestUpdatesSetting {
    GroupedChapterList = 1,
    #[serde(other)]
    Default = 0,
}

impl Default for LatestUpdatesSetting {
    fn default() -> Self {
        0u8.into()
    }
}

impl std::fmt::Display for LatestUpdatesSetting {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_str(match self {
            Self::Default => "Default",
            Self::GroupedChapterList => "Grouped Chapter List",
        })
    }
}

impl From<u8> for LatestUpdatesSetting {
    fn from(value: u8) -> Self {
        match value {
            0 => Self::Default,
            1 => Self::GroupedChapterList,
            _ => Self::Default,
        }
    }
}

impl From<u16> for LatestUpdatesSetting {
    fn from(value: u16) -> Self {
        match value {
            0 => Self::Default,
            1 => Self::GroupedChapterList,
            _ => Self::Default,
        }
    }
}

impl From<u32> for LatestUpdatesSetting {
    fn from(value: u32) -> Self {
        match value {
            0 => Self::Default,
            1 => Self::GroupedChapterList,
            _ => Self::Default,
        }
    }
}

impl From<u64> for LatestUpdatesSetting {
    fn from(value: u64) -> Self {
        match value {
            0 => Self::Default,
            1 => Self::GroupedChapterList,
            _ => Self::Default,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn latest_updates_setting_produces_default_from_default() {
        let latest_updates_setting = LatestUpdatesSetting::default();
        assert_eq!(latest_updates_setting, LatestUpdatesSetting::Default);
    }

    #[test]
    fn latest_updates_produces_grouped_chapter_list_from_1() {
        let latest_updates_setting = LatestUpdatesSetting::from(1u8);
        assert_eq!(
            latest_updates_setting,
            LatestUpdatesSetting::GroupedChapterList
        );
    }
}
