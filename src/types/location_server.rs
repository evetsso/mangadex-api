use serde::{Deserialize, Serialize};

/// Server types for overriding location-based server assignments.
#[derive(Clone, Copy, Debug, Deserialize, Hash, PartialEq, PartialOrd, Serialize)]
#[repr(u8)]
#[serde(from = "&str")]
pub enum LocationServer {
    /// Supposedly represents EU as well.
    #[serde(rename = "na")]
    NorthAmerica = 1,
    /// Supposedly represents EU2 as well.
    #[serde(rename = "na2")]
    NorthAmerica2 = 2,
    /// Don't override the location-based server assignment.
    #[serde(other)]
    Worldwide = 0,
}

impl LocationServer {
    /// Return the MangaDex representation of the server for querying.
    pub fn mangadex_repr(&self) -> &str {
        match self {
            Self::NorthAmerica => "na",
            Self::NorthAmerica2 => "na2",
            Self::Worldwide => "",
        }
    }
}

impl From<&str> for LocationServer {
    fn from(value: &str) -> Self {
        match value {
            "na" => Self::NorthAmerica,
            "na2" => Self::NorthAmerica2,
            _ => Self::Worldwide,
        }
    }
}
