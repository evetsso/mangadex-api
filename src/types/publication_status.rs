use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, Hash, PartialEq, PartialOrd, Serialize)]
#[serde(from = "u8")]
#[repr(u8)]
pub enum PublicationStatus {
    Ongoing = 1,
    Completed = 2,
    Cancelled = 3,
    Hiatus = 4,
    #[serde(other)]
    Unknown = 0,
}

impl From<u8> for PublicationStatus {
    fn from(value: u8) -> Self {
        match value {
            1 => Self::Ongoing,
            2 => Self::Completed,
            3 => Self::Cancelled,
            4 => Self::Hiatus,
            _ => Self::Unknown,
        }
    }
}

impl From<u16> for PublicationStatus {
    fn from(value: u16) -> Self {
        match value {
            1 => Self::Ongoing,
            2 => Self::Completed,
            3 => Self::Cancelled,
            4 => Self::Hiatus,
            _ => Self::Unknown,
        }
    }
}

impl From<u32> for PublicationStatus {
    fn from(value: u32) -> Self {
        match value {
            1 => Self::Ongoing,
            2 => Self::Completed,
            3 => Self::Cancelled,
            4 => Self::Hiatus,
            _ => Self::Unknown,
        }
    }
}

impl From<u64> for PublicationStatus {
    fn from(value: u64) -> Self {
        match value {
            1 => Self::Ongoing,
            2 => Self::Completed,
            3 => Self::Cancelled,
            4 => Self::Hiatus,
            _ => Self::Unknown,
        }
    }
}
