use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, Hash, PartialEq, PartialOrd, Serialize)]
#[serde(from = "u8")]
#[repr(u8)]
pub enum Demographic {
    Shounen = 1,
    Shoujo = 2,
    Seinen = 3,
    Josei = 4,
    #[serde(other)]
    None = 0,
}

impl From<u8> for Demographic {
    fn from(value: u8) -> Self {
        match value {
            1 => Self::Shounen,
            2 => Self::Shoujo,
            3 => Self::Seinen,
            4 => Self::Josei,
            _ => Self::None,
        }
    }
}

impl From<u16> for Demographic {
    fn from(value: u16) -> Self {
        match value {
            1 => Self::Shounen,
            2 => Self::Shoujo,
            3 => Self::Seinen,
            4 => Self::Josei,
            _ => Self::None,
        }
    }
}

impl From<u32> for Demographic {
    fn from(value: u32) -> Self {
        match value {
            1 => Self::Shounen,
            2 => Self::Shoujo,
            3 => Self::Seinen,
            4 => Self::Josei,
            _ => Self::None,
        }
    }
}

impl From<u64> for Demographic {
    fn from(value: u64) -> Self {
        match value {
            1 => Self::Shounen,
            2 => Self::Shoujo,
            3 => Self::Seinen,
            4 => Self::Josei,
            _ => Self::None,
        }
    }
}
