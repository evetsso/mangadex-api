use serde::{Deserialize, Serialize};

/// Chapter status types.
#[derive(Clone, Copy, Debug, Deserialize, Hash, PartialEq, PartialOrd, Serialize)]
#[repr(u8)]
#[serde(from = "&str")]
pub enum ChapterStatus {
    /// Chapter is available.
    Ok = 1,
    /// Chapter will be available in the future.
    Delayed = 2,
    /// This chapter can be read for free on the official publisher's website.
    External = 3,
    /// Unknown status.
    #[serde(other)]
    Other = 0,
}

impl ChapterStatus {
    /// Return the MangaDex chapter status representation.
    pub fn mangadex_repr(&self) -> &str {
        match self {
            Self::Ok => "OK",
            Self::Delayed => "delayed",
            Self::External => "external",
            Self::Other => "",
        }
    }
}

impl From<&str> for ChapterStatus {
    fn from(value: &str) -> Self {
        match value.to_lowercase().as_str() {
            "ok" => Self::Ok,
            "delayed" => Self::Delayed,
            "external" => Self::External,
            _ => Self::Other,
        }
    }
}
