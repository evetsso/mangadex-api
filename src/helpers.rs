use crate::types::MangaId;

/// Attempt to parse a manga ID from a web scraped URL slug.
///
/// URL format is "/manga/{manga_id}/{manga_title}",
/// so the desired index is 1 after stripping the left slash.
pub fn parse_manga_id_from_scraped_url(url: &str) -> Option<MangaId> {
    match url.strip_prefix('/') {
        Some(u) => match u.split('/').collect::<Vec<&str>>()[1].parse::<MangaId>() {
            Ok(manga_id) => Some(manga_id),
            Err(_) => None,
        },
        None => None,
    }
}
