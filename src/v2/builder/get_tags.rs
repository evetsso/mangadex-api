use reqwest::Error;

use crate::v2::responses::TagsResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/tag` endpoint.
pub struct GetTagsBuilder<'a> {
    md_client: &'a MangaDexV2,
}

impl<'a> GetTagsBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2) -> Self {
        Self { md_client }
    }

    pub async fn send(&self) -> Result<TagsResponseType, Error> {
        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/tag",
                base_url = self.md_client.api_url()
            ))
            .send()
            .await?
            .json::<TagsResponseType>()
            .await?)
    }
}
