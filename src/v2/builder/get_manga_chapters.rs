use reqwest::Error;

use crate::types::MangaId;
use crate::v2::responses::MangaChaptersResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/manga/{id}/chapters` endpoint.
pub struct GetMangaChaptersBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: MangaId,
    page: Option<u32>,
    limit: Option<u32>,
}

impl<'a> GetMangaChaptersBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2, id: MangaId) -> Self {
        Self {
            md_client,
            id,
            page: None,
            limit: None,
        }
    }

    /// Set the current page of the paginated results.
    ///
    /// Default returns all chapters without pagination.
    pub fn page(&mut self, page: u32) -> &mut Self {
        self.page = Some(page);
        self
    }

    /// Set the limit of the paginated results.
    ///
    /// - **Minimum**: 10
    /// - **Maximum**: 100
    /// - **Default**: 100
    pub fn limit(&mut self, limit: u32) -> &mut Self {
        self.limit = Some(limit);
        self
    }

    pub async fn send(&self) -> Result<MangaChaptersResponseType, Error> {
        let mut params = Vec::new();
        if let Some(page) = self.page {
            params.push(("p", page));
        }
        if let Some(limit) = self.limit {
            params.push(("limit", limit));
        }

        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/manga/{manga_id}/chapters",
                base_url = self.md_client.api_url(),
                manga_id = self.id
            ))
            .query(&params)
            .send()
            .await?
            .json::<MangaChaptersResponseType>()
            .await?)
    }
}
