use crate::constants::USER_ME;
use crate::types::UserId;
use crate::v2::builder::UserRequestError;
use crate::v2::responses::UserFollowedMangaResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/user/{id}/followed-manga` endpoint.
pub struct GetUserFollowedMangaBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: Option<UserId>,
    me: bool,
}

impl<'a> GetUserFollowedMangaBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2) -> Self {
        Self {
            md_client,
            id: None,
            me: false,
        }
    }

    /// Set the numeric user ID.
    ///
    /// This takes priority over the `me` field if both are set.
    pub fn user_id(&mut self, id: UserId) -> &mut Self {
        self.id = Some(id);
        self
    }

    /// Set `true` to use the current cookie-authenticated user.
    pub fn me(&mut self, me: bool) -> &mut Self {
        self.me = me;
        self
    }

    pub async fn send(&self) -> Result<UserFollowedMangaResponseType, UserRequestError> {
        let id = if let Some(id) = self.id {
            id.to_string()
        } else if self.me {
            String::from(USER_ME)
        } else {
            return Err(UserRequestError::IDNotProvidedError);
        };

        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/user/{user_id}/followed-manga",
                base_url = self.md_client.api_url(),
                user_id = id
            ))
            .send()
            .await?
            .json::<UserFollowedMangaResponseType>()
            .await?)
    }
}
