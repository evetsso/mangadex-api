use std::collections::HashMap;

use crate::types::GroupId;
use crate::v2::responses::GroupResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/group/{id}` endpoint.
pub struct GetGroupBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: GroupId,
    include: Option<String>,
}

impl<'a> GetGroupBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2, id: GroupId) -> Self {
        Self {
            md_client,
            id,
            include: None,
        }
    }

    /// Include additional information with the group.
    ///
    /// # Available values
    ///
    /// - `chapters`: Include partial chapter information.
    pub fn include<T: Into<String>>(&mut self, include: T) -> &mut Self {
        self.include = Some(include.into());
        self
    }

    /// Convenience method to include partial chapter information with the user.
    pub fn include_chapters(&mut self) -> &mut Self {
        self.include("chapters")
    }

    /// Send the request to MangaDex with the inputted parameters.
    pub async fn send(&self) -> Result<GroupResponseType, reqwest::Error> {
        let mut params = HashMap::new();
        if let Some(include) = &self.include {
            params.insert("include", include);
        }

        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/group/{group_id}",
                base_url = self.md_client.api_url(),
                group_id = self.id
            ))
            .query(&params)
            .send()
            .await?
            .json::<GroupResponseType>()
            .await?)
    }
}
