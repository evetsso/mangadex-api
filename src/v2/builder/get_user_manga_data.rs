use crate::constants::USER_ME;
use crate::types::{MangaId, UserId};
use crate::v2::builder::UserRequestError;
use crate::v2::responses::UserMangaDataResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/user/{user_id}/manga/{manga_id}` endpoint.
pub struct GetUserMangaDataBuilder<'a> {
    md_client: &'a MangaDexV2,
    user_id: Option<UserId>,
    me: bool,
    manga_id: MangaId,
}

impl<'a> GetUserMangaDataBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2, manga_id: MangaId) -> Self {
        Self {
            md_client,
            user_id: None,
            me: false,
            manga_id,
        }
    }

    /// Set the numeric user ID.
    ///
    /// This takes priority over the `me` field if both are set.
    pub fn user_id(&mut self, user_id: UserId) -> &mut Self {
        self.user_id = Some(user_id);
        self
    }

    /// Set `true` to use the current cookie-authenticated user.
    pub fn me(&mut self, me: bool) -> &mut Self {
        self.me = me;
        self
    }

    pub async fn send(&self) -> Result<UserMangaDataResponseType, UserRequestError> {
        let user_id = if let Some(user_id) = self.user_id {
            user_id.to_string()
        } else if self.me {
            String::from(USER_ME)
        } else {
            return Err(UserRequestError::IDNotProvidedError);
        };

        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/user/{user_id}/manga/{manga_id}",
                base_url = self.md_client.api_url(),
                user_id = user_id,
                manga_id = self.manga_id
            ))
            .send()
            .await?
            .json::<UserMangaDataResponseType>()
            .await?)
    }
}
