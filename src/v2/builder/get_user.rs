use std::collections::HashMap;

use crate::constants::USER_ME;
use crate::types::UserId;
use crate::v2::builder::UserRequestError;
use crate::v2::responses::UserResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/user/{id}` endpoint.
pub struct GetUserBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: Option<UserId>,
    me: bool,
    include: Option<String>,
}

impl<'a> GetUserBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2) -> Self {
        Self {
            md_client,
            id: None,
            me: false,
            include: None,
        }
    }

    /// Set the numeric user ID.
    ///
    /// This takes priority over the `me` field if both are set.
    pub fn user_id(&mut self, id: UserId) -> &mut Self {
        self.id = Some(id);
        self
    }

    /// Set `true` to use the current cookie-authenticated user.
    pub fn me(&mut self, me: bool) -> &mut Self {
        self.me = me;
        self
    }

    /// Include additional information with the user.
    ///
    /// # Available values
    ///
    /// - `chapters`: Include partial chapter information.
    pub fn include<T: Into<String>>(&mut self, include: T) -> &mut Self {
        self.include = Some(include.into());
        self
    }

    /// Convenience method to include partial chapter information with the user.
    pub fn include_chapters(&mut self) -> &mut Self {
        self.include("chapters")
    }

    pub async fn send(&self) -> Result<UserResponseType, UserRequestError> {
        let id = if let Some(id) = self.id {
            id.to_string()
        } else if self.me {
            String::from(USER_ME)
        } else {
            return Err(UserRequestError::IDNotProvidedError);
        };

        let mut params = HashMap::new();
        if let Some(include) = &self.include {
            params.insert("include", include);
        }

        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/user/{user_id}",
                base_url = self.md_client.api_url(),
                user_id = id
            ))
            .query(&params)
            .send()
            .await?
            .json::<UserResponseType>()
            .await?)
    }
}
