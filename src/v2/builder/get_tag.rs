use reqwest::Error;

use crate::types::TagId;
use crate::v2::responses::TagResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/tag/{id}` endpoint.
pub struct GetTagBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: TagId,
}

impl<'a> GetTagBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2, id: TagId) -> Self {
        Self { md_client, id }
    }

    pub async fn send(&self) -> Result<TagResponseType, Error> {
        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/tag/{tag_id}",
                base_url = self.md_client.api_url(),
                tag_id = self.id
            ))
            .send()
            .await?
            .json::<TagResponseType>()
            .await?)
    }
}
