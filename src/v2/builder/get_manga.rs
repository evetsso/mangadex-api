use reqwest::Error;

use crate::types::MangaId;
use crate::v2::responses::MangaResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/manga/{id}` endpoint.
pub struct GetMangaBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: MangaId,
}

impl<'a> GetMangaBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2, id: MangaId) -> Self {
        Self { md_client, id }
    }

    pub async fn send(&self) -> Result<MangaResponseType, Error> {
        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/manga/{manga_id}",
                base_url = self.md_client.api_url(),
                manga_id = self.id
            ))
            .send()
            .await?
            .json::<MangaResponseType>()
            .await?)
    }
}
