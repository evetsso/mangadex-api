use reqwest::Error;

use crate::v2::responses::MangaRelationTypesResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/relations` endpoint.
pub struct GetMangaRelationTypesBuilder<'a> {
    md_client: &'a MangaDexV2,
}

impl<'a> GetMangaRelationTypesBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2) -> Self {
        Self { md_client }
    }

    pub async fn send(&self) -> Result<MangaRelationTypesResponseType, Error> {
        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/relations",
                base_url = self.md_client.api_url()
            ))
            .send()
            .await?
            .json::<MangaRelationTypesResponseType>()
            .await?)
    }
}
