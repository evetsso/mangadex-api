use reqwest::Error;

use crate::types::GroupId;
use crate::v2::responses::GroupChaptersResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/group/{id}/chapters` endpoint.
pub struct GetGroupChaptersBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: GroupId,
    page: Option<u32>,
    limit: Option<u32>,
}

impl<'a> GetGroupChaptersBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2, id: GroupId) -> Self {
        Self {
            md_client,
            id,
            page: None,
            limit: None,
        }
    }

    /// Set the current page of the paginated results.
    ///
    /// Default returns all chapters without pagination.
    pub fn page(&mut self, page: u32) -> &mut Self {
        self.page = Some(page);
        self
    }

    /// Set the limit of the paginated results.
    ///
    /// - **Minimum**: 10
    /// - **Maximum**: 100
    /// - **Default**: 100
    pub fn limit(&mut self, limit: u32) -> &mut Self {
        self.limit = Some(limit);
        self
    }

    pub async fn send(&self) -> Result<GroupChaptersResponseType, Error> {
        let mut params = Vec::new();
        if let Some(page) = self.page {
            params.push(("p", page));
        }
        if let Some(limit) = self.limit {
            params.push(("limit", limit));
        }

        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/group/{group_id}/chapters",
                base_url = self.md_client.api_url(),
                group_id = self.id
            ))
            .query(&params)
            .send()
            .await?
            .json::<GroupChaptersResponseType>()
            .await?)
    }
}
