use reqwest::Error;

use crate::v2::responses::FollowTypesResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/follows` endpoint.
pub struct GetFollowTypesBuilder<'a> {
    md_client: &'a MangaDexV2,
}

impl<'a> GetFollowTypesBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2) -> Self {
        Self { md_client }
    }

    pub async fn send(&self) -> Result<FollowTypesResponseType, Error> {
        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/follows",
                base_url = self.md_client.api_url()
            ))
            .send()
            .await?
            .json::<FollowTypesResponseType>()
            .await?)
    }
}
