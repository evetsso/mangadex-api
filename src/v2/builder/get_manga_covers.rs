use reqwest::Error;

use crate::types::MangaId;
use crate::v2::responses::MangaCoversResponseType;
use crate::v2::MangaDexV2;
use crate::MangaDexClient;

/// Builder for the `/api/v2/manga/{id}/covers` endpoint.
pub struct GetMangaCoversBuilder<'a> {
    md_client: &'a MangaDexV2,
    id: MangaId,
}

impl<'a> GetMangaCoversBuilder<'a> {
    pub fn new(md_client: &'a MangaDexV2, id: MangaId) -> Self {
        Self { md_client, id }
    }

    /// Send the request to MangaDex with the inputted parameters.
    pub async fn send(&self) -> Result<MangaCoversResponseType, Error> {
        Ok(self
            .md_client
            .client()
            .get(&format!(
                "{base_url}/manga/{manga_id}/covers",
                base_url = self.md_client.api_url(),
                manga_id = self.id
            ))
            .send()
            .await?
            .json::<MangaCoversResponseType>()
            .await?)
    }
}
