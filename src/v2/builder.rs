mod get_chapter;
mod get_follow_types;
mod get_group;
mod get_group_chapters;
mod get_manga;
mod get_manga_chapters;
mod get_manga_covers;
mod get_manga_relation_types;
mod get_tag;
mod get_tags;
mod get_user;
mod get_user_chapters;
mod get_user_followed_manga;
mod get_user_followed_updates;
mod get_user_manga_data;
mod get_user_manga_ratings;
mod get_user_settings;
mod set_user_chapters_read;

use reqwest::Error;

pub use get_chapter::{GetChapterBuilder, GetChapterError};
pub use get_follow_types::GetFollowTypesBuilder;
pub use get_group::GetGroupBuilder;
pub use get_group_chapters::GetGroupChaptersBuilder;
pub use get_manga::GetMangaBuilder;
pub use get_manga_chapters::GetMangaChaptersBuilder;
pub use get_manga_covers::GetMangaCoversBuilder;
pub use get_manga_relation_types::GetMangaRelationTypesBuilder;
pub use get_tag::GetTagBuilder;
pub use get_tags::GetTagsBuilder;
pub use get_user::GetUserBuilder;
pub use get_user_chapters::GetUserChaptersBuilder;
pub use get_user_followed_manga::GetUserFollowedMangaBuilder;
pub use get_user_followed_updates::GetUserFollowedUpdatesBuilder;
pub use get_user_manga_data::GetUserMangaDataBuilder;
pub use get_user_manga_ratings::GetUserMangaRatingsBuilder;
pub use get_user_settings::GetUserSettingsBuilder;
pub use set_user_chapters_read::SetUserChaptersReadBuilder;

#[derive(Debug)]
pub enum UserRequestError {
    IDNotProvidedError,
    RequestError(Error),
    ParseError(Error),
}

impl From<Error> for UserRequestError {
    fn from(error: Error) -> Self {
        if error.is_body() {
            Self::ParseError(error)
        } else {
            Self::RequestError(error)
        }
    }
}
