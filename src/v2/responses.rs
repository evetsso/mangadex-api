use serde::Deserialize;

mod chapter;
mod follow_types;
mod group;
mod group_chapters;
mod manga;
mod manga_chapters;
mod manga_covers;
mod manga_relation_types;
pub mod partials;
mod tag;
mod tags;
mod user;
mod user_chapters;
mod user_chapters_read;
mod user_followed_manga;
mod user_followed_updates;
mod user_manga_data;
mod user_manga_ratings;
mod user_settings;

use crate::types::{MangaDexResponseStatus, StatusCode};
pub use chapter::ChapterResponseType;
pub use follow_types::FollowTypesResponseType;
pub use group::GroupResponseType;
pub use group_chapters::GroupChaptersResponseType;
pub use manga::MangaResponseType;
pub use manga_chapters::MangaChaptersResponseType;
pub use manga_covers::MangaCoversResponseType;
pub use manga_relation_types::MangaRelationTypesResponseType;
pub use tag::TagResponseType;
pub use tags::TagsResponseType;
pub use user::UserResponseType;
pub use user_chapters::UserChaptersResponseType;
pub use user_chapters_read::UserChaptersMarkerResponseType;
pub use user_followed_manga::UserFollowedMangaResponseType;
pub use user_followed_updates::UserFollowedUpdatesResponseType;
pub use user_manga_data::UserMangaDataResponseType;
pub use user_manga_ratings::UserMangaRatingsResponseType;
pub use user_settings::UserSettingsResponseType;

pub trait ResponseType {
    type OkOutput;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool;

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput>;
}

pub trait ResponseBody {}

#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct ErrorResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    message: String,
}

impl ResponseBody for ErrorResponse {}
