use std::collections::HashMap;

use serde::Deserialize;

use crate::types::{MangaDexResponseStatus, StatusCode};
use crate::v2::responses::{ErrorResponse, ResponseBody, ResponseType};

#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(untagged)]
pub enum MangaRelationTypesResponseType {
    Ok(MangaRelationTypesResponse),
    Err(ErrorResponse),
}

impl ResponseType for MangaRelationTypesResponseType {
    type OkOutput = MangaRelationTypesResponse;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool {
        matches!(self, Self::Ok(_))
    }

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput> {
        match self {
            Self::Ok(data) => Some(data),
            _ => None,
        }
    }
}

/// Response from the `/api/v2/relations` endpoint.
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub struct MangaRelationTypesResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    data: HashMap<String, MangaRelationTypeData>,
}

impl MangaRelationTypesResponse {
    pub fn code(&self) -> &StatusCode {
        &self.code
    }

    pub fn status(&self) -> &MangaDexResponseStatus {
        &self.status
    }

    pub fn data(&self) -> &HashMap<String, MangaRelationTypeData> {
        &self.data
    }
}

impl ResponseBody for MangaRelationTypesResponse {}

/// This struct should not be used directly.
#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct MangaRelationTypeData {
    // Intentionally not deserializing to the RelatedMangaType enum variant.
    id: u8,
    name: String,
    pair_id: u8,
}

impl MangaRelationTypeData {
    pub fn id(&self) -> &u8 {
        &self.id
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn pair_id(&self) -> &u8 {
        &self.pair_id
    }
}
