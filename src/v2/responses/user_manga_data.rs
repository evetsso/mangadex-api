use serde::Deserialize;

use crate::types::{MangaDexResponseStatus, StatusCode};
use crate::v2::responses::partials::UserMangaPartial;
use crate::v2::responses::{ErrorResponse, ResponseBody, ResponseType};

#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
#[serde(untagged)]
pub enum UserMangaDataResponseType {
    Ok(UserMangaDataResponse),
    Err(ErrorResponse),
}

impl ResponseType for UserMangaDataResponseType {
    type OkOutput = UserMangaDataResponse;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool {
        matches!(self, Self::Ok(_))
    }

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput> {
        match self {
            Self::Ok(data) => Some(data),
            _ => None,
        }
    }
}

/// User's data for a manga.
///
/// This is the response from the `/api/v2/user/{user_id}/manga/{manga_id}` endpoint.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct UserMangaDataResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    data: UserMangaPartial,
}

impl UserMangaDataResponse {
    pub fn code(&self) -> &StatusCode {
        &self.code
    }

    pub fn status(&self) -> &MangaDexResponseStatus {
        &self.status
    }

    pub fn data(&self) -> &UserMangaPartial {
        &self.data
    }
}

impl ResponseBody for UserMangaDataResponse {}
