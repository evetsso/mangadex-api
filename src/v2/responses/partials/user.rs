use serde::Deserialize;

use crate::types::UserId;

/// Partial information about a user.
///
/// This struct should not be used directly.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct UserPartial {
    id: UserId,
    name: String,
}

impl UserPartial {
    pub fn id(&self) -> &UserId {
        &self.id
    }

    pub fn name(&self) -> &String {
        &self.name
    }
}
