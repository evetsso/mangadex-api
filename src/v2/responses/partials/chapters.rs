use serde::Deserialize;

use crate::v2::responses::partials::{ChapterPartial, GroupPartial};

/// This struct should not be used directly.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct ChaptersDataPartial {
    chapters: Vec<ChapterPartial>,
    groups: Vec<GroupPartial>,
}

impl ChaptersDataPartial {
    pub fn chapters(&self) -> &Vec<ChapterPartial> {
        &self.chapters
    }

    pub fn groups(&self) -> &Vec<GroupPartial> {
        &self.groups
    }
}
