use serde::Deserialize;

use crate::types::GroupId;

/// Partial information about a group.
///
/// This struct should not be used directly.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct GroupPartial {
    id: GroupId,
    name: String,
}

impl GroupPartial {
    pub fn id(&self) -> &GroupId {
        &self.id
    }

    pub fn name(&self) -> &String {
        &self.name
    }
}
