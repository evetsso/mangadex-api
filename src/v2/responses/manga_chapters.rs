use serde::Deserialize;

use crate::types::{MangaDexResponseStatus, StatusCode};
use crate::v2::responses::partials::PartialChapterData;
use crate::v2::responses::{ErrorResponse, ResponseBody, ResponseType};

#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
#[serde(untagged)]
pub enum MangaChaptersResponseType {
    Ok(MangaChaptersResponse),
    Err(ErrorResponse),
}

impl ResponseType for MangaChaptersResponseType {
    type OkOutput = MangaChaptersResponse;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool {
        matches!(self, Self::Ok(_))
    }

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput> {
        match self {
            Self::Ok(data) => Some(data),
            _ => None,
        }
    }
}

/// Partial information about the chapters belonging to a manga.
///
/// This is the response from the `/api/v2/manga/{id}/chapters` endpoint.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct MangaChaptersResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    data: PartialChapterData,
}

impl MangaChaptersResponse {
    pub fn code(&self) -> &StatusCode {
        &self.code
    }

    pub fn status(&self) -> &MangaDexResponseStatus {
        &self.status
    }

    pub fn data(&self) -> &PartialChapterData {
        &self.data
    }
}

impl ResponseBody for MangaChaptersResponse {}
