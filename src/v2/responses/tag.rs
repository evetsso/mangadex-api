use serde::Deserialize;

use crate::types::{MangaDexResponseStatus, StatusCode};
use crate::v2::responses::partials::TagData;
use crate::v2::responses::{ErrorResponse, ResponseBody, ResponseType};

#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
#[serde(untagged)]
pub enum TagResponseType {
    Ok(TagResponse),
    Err(ErrorResponse),
}

impl ResponseType for TagResponseType {
    type OkOutput = TagResponse;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool {
        matches!(self, Self::Ok(_))
    }

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput> {
        match self {
            Self::Ok(data) => Some(data),
            _ => None,
        }
    }
}

/// Response from the `/api/v2/tag/{id}` endpoint.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct TagResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    data: TagData,
}

impl TagResponse {
    pub fn code(&self) -> &StatusCode {
        &self.code
    }

    pub fn status(&self) -> &MangaDexResponseStatus {
        &self.status
    }

    pub fn data(&self) -> &TagData {
        &self.data
    }
}

impl ResponseBody for TagResponse {}
