use std::collections::HashMap;

use serde::Deserialize;

use crate::types::{MangaDexResponseStatus, StatusCode};
use crate::v2::responses::partials::TagData;
use crate::v2::responses::{ErrorResponse, ResponseBody, ResponseType};

#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(untagged)]
pub enum TagsResponseType {
    Ok(TagsResponse),
    Err(ErrorResponse),
}

impl ResponseType for TagsResponseType {
    type OkOutput = TagsResponse;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool {
        matches!(self, Self::Ok(_))
    }

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput> {
        match self {
            Self::Ok(data) => Some(data),
            _ => None,
        }
    }
}

/// Response from the `/api/v2/tag` endpoint.
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub struct TagsResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    data: HashMap<String, TagData>,
}

impl TagsResponse {
    pub fn code(&self) -> &StatusCode {
        &self.code
    }

    pub fn status(&self) -> &MangaDexResponseStatus {
        &self.status
    }

    pub fn data(&self) -> &HashMap<String, TagData> {
        &self.data
    }
}

impl ResponseBody for TagsResponse {}
