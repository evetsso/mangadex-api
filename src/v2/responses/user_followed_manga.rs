use serde::Deserialize;

use crate::types::{MangaDexResponseStatus, StatusCode};
use crate::v2::responses::partials::UserMangaPartial;
use crate::v2::responses::{ErrorResponse, ResponseType};

#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
#[serde(untagged)]
pub enum UserFollowedMangaResponseType {
    Ok(UserFollowedMangaResponse),
    Error(ErrorResponse),
}

impl ResponseType for UserFollowedMangaResponseType {
    type OkOutput = UserFollowedMangaResponse;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool {
        matches!(self, Self::Ok(_))
    }

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput> {
        match self {
            Self::Ok(data) => Some(data),
            _ => None,
        }
    }
}

/// Information about the user's followed manga.
///
/// This is the response from the `/api/v2/user/{id}/followed-manga`.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
pub struct UserFollowedMangaResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    data: Vec<UserMangaPartial>,
}

impl UserFollowedMangaResponse {
    pub fn code(&self) -> &StatusCode {
        &self.code
    }

    pub fn status(&self) -> &MangaDexResponseStatus {
        &self.status
    }

    pub fn data(&self) -> &Vec<UserMangaPartial> {
        &self.data
    }
}
