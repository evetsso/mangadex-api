use std::collections::HashMap;

use serde::Deserialize;

use crate::types::{MangaDexResponseStatus, StatusCode};
use crate::v2::responses::{ErrorResponse, ResponseBody, ResponseType};

#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(untagged)]
pub enum FollowTypesResponseType {
    Ok(FollowTypesResponse),
    Err(ErrorResponse),
}

impl ResponseType for FollowTypesResponseType {
    type OkOutput = FollowTypesResponse;

    /// Check if the endpoint response is ok.
    fn is_ok(&self) -> bool {
        matches!(self, Self::Ok(_))
    }

    /// Get the data from the endpoint if the response is ok.
    fn ok(self) -> Option<Self::OkOutput> {
        match self {
            Self::Ok(data) => Some(data),
            _ => None,
        }
    }
}

/// Response from the `/api/v2/follows` endpoint.
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub struct FollowTypesResponse {
    code: StatusCode,
    status: MangaDexResponseStatus,
    data: HashMap<String, FollowTypeData>,
}

impl FollowTypesResponse {
    pub fn code(&self) -> &StatusCode {
        &self.code
    }

    pub fn status(&self) -> &MangaDexResponseStatus {
        &self.status
    }

    pub fn data(&self) -> &HashMap<String, FollowTypeData> {
        &self.data
    }
}

impl ResponseBody for FollowTypesResponse {}

/// This struct should not be used directly.
#[derive(Debug, Deserialize, Clone, PartialOrd, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct FollowTypeData {
    id: u8,
    // Intentionally not deserializing to the FollowStatus enum variant.
    name: String,
}

impl FollowTypeData {
    pub fn id(&self) -> &u8 {
        &self.id
    }

    pub fn name(&self) -> &String {
        &self.name
    }
}
