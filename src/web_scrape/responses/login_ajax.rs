#[derive(Debug, PartialEq)]
pub enum LoginAjaxResponseType {
    Ok,
    TwoFactorAuthenticationRequired,
    IncorrectUsernameOrPassword,
    IncorrectTwoFactorCodeLength,
    FailedTwoFactorVerification,
    Error(String),
}

impl LoginAjaxResponseType {
    /// Translate the response body from MangaDex's login AJAX endpoint to the internal response
    /// representation.
    ///
    /// The response content type is "text/html".
    pub fn from_response_body(s: &str) -> Self {
        if s.is_empty() {
            Self::Ok
        } else if s == "missing_2fa" {
            Self::TwoFactorAuthenticationRequired
        } else if s.contains("Incorrect username or password") {
            Self::IncorrectUsernameOrPassword
        } else if s.contains("Incorrect code length") {
            Self::IncorrectTwoFactorCodeLength
        } else if s.contains("Failed to verify logincode") {
            Self::FailedTwoFactorVerification
        } else {
            Self::Error(s.to_string())
        }
    }
}
