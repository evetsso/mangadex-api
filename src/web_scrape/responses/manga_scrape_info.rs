use crate::types::MangaId;

/// Partial manga information from web-scraped data.
#[derive(Clone, Debug, Eq, Hash, PartialEq, PartialOrd)]
pub struct MangaScrapeInfo {
    manga_id: MangaId,
    title: String,
    cover_url: String,
}

impl MangaScrapeInfo {
    pub fn new<T, U>(base_url: U, manga_id: MangaId, title: T, use_thumbnail: bool) -> Self
    where
        T: Into<String>,
        U: Into<String>,
    {
        Self {
            manga_id,
            title: title.into(),
            cover_url: format!(
                "{base_url}{cover_url}",
                base_url = base_url.into(),
                cover_url = form_cover_url(manga_id, use_thumbnail)
            ),
        }
    }

    /// Return the manga ID.
    pub fn manga_id(&self) -> &MangaId {
        &self.manga_id
    }

    /// Return the manga title.
    pub fn title(&self) -> &String {
        &self.title
    }

    /// Return the manga cover image URL.
    pub fn cover_url(&self) -> &String {
        &self.cover_url
    }
}

fn form_cover_url(manga_id: MangaId, use_thumbnail: bool) -> String {
    format!(
        "/images/manga/{manga_id}{ext}",
        manga_id = manga_id,
        ext = format!(
            "{thumb}{ext}",
            thumb = if use_thumbnail { ".thumb" } else { "" },
            ext = ".jpg"
        )
    )
}
