mod get_latest_updates;
mod get_most_popular_manga;
mod login_ajax;
mod search_manga;

pub use get_latest_updates::GetLatestUpdatesScrapeBuilder;
pub use get_most_popular_manga::GetMostPopularMangaScrapeBuilder;
pub use login_ajax::LoginAjaxBuilder;
pub use search_manga::SearchMangaScrapeBuilder;
