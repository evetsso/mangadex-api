mod login_ajax;
mod manga_scrape_info;

pub use login_ajax::LoginAjaxResponseType;
pub use manga_scrape_info::MangaScrapeInfo;
