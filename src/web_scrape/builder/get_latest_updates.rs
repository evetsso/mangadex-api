use scraper::{Html, Selector};

use crate::cookie::MangaDexCookie;
use crate::helpers::parse_manga_id_from_scraped_url;
use crate::types::Language;
use crate::web_scrape::responses::MangaScrapeInfo;
use crate::MangaDexClient;

/// Builder for the `/updates/{page}` endpoint.
///
/// By default, 50 results are returned on the page.
pub struct GetLatestUpdatesScrapeBuilder<'a> {
    md_client: &'a (dyn MangaDexClient + 'a),
    page: u32,
    chapter_languages: Vec<Language>,
    use_cover_thumbnail: bool,
}

impl<'a> GetLatestUpdatesScrapeBuilder<'a> {
    /// Create a new builder instance for fetching the latest chapter updates.
    pub fn new(md_client: &'a dyn MangaDexClient) -> GetLatestUpdatesScrapeBuilder<'a> {
        Self {
            md_client,
            page: 1,
            chapter_languages: Vec::new(),
            use_cover_thumbnail: false,
        }
    }

    /// Set the page to fetch.
    pub fn page(&mut self, page: u32) -> &mut Self {
        self.page = page;
        self
    }

    /// Filter the chapter languages.
    pub fn chapter_languages(&mut self, chapter_languages: Vec<Language>) -> &mut Self {
        self.chapter_languages = chapter_languages;
        self
    }

    /// Choose to use the thumbnail version of the cover image.
    ///
    /// If false, it will use the full-resolution image.
    pub fn use_cover_thumbnail(&mut self, use_cover_thumbnail: bool) -> &mut Self {
        self.use_cover_thumbnail = use_cover_thumbnail;
        self
    }

    /// Send the request to MangaDex.
    pub async fn send(&self) -> Result<Vec<MangaScrapeInfo>, reqwest::Error> {
        let mut req = self.md_client.client().get(&format!(
            "{base_url}/updates/{page}",
            base_url = self.md_client.base_url(),
            page = self.page
        ));

        if !self.chapter_languages.is_empty() {
            req = req.header(
                reqwest::header::COOKIE,
                MangaDexCookie::FilterChapterLanguages(self.chapter_languages.clone()).to_string(),
            );
        }

        let resp = req.send().await?;

        let body = resp.text().await?;
        let fragment = Html::parse_document(&body);
        // Selecting the chapter URL is difficult since there are no unique identifiers.
        let title_selector = Selector::parse("a.manga_title").unwrap();

        Ok(fragment
            .select(&title_selector)
            .filter_map(|el| {
                let title: String = el.text().collect();
                let url = el.value().attr("href")?;
                let manga_id = parse_manga_id_from_scraped_url(url)?;

                Some(MangaScrapeInfo::new(
                    self.md_client.base_url(),
                    manga_id,
                    title,
                    self.use_cover_thumbnail,
                ))
            })
            .collect())
    }
}
