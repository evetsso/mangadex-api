//! Download the pages of a chapter.
//!
//! This only accepts the numerical chapter ID, not the chapter hash.
//!
//! The page filenames have the following format: `{page_num}-{hash}.jpg`.
//! In a more thorough program, it is probably desirable to use the "{page_index}+1.jpg" naming
//! convention where `page_index` starts from 0.

use std::fs::{create_dir, File};
use std::io::Write;
use std::path::{Path, PathBuf};

use reqwest::{Client, Error};
use structopt::StructOpt;

use mangadex_api::types::ChapterId;
use mangadex_api::v2::responses::ResponseType;
use mangadex_api::v2::MangaDexV2;
use mangadex_api::MangaDexClient;

#[derive(StructOpt)]
#[structopt(
    name = "MangaDex Chapter Downloader",
    about = "Download the chapter's pages."
)]
struct Opt {
    /// Numerical chapter ID.
    #[structopt()]
    chapter_id: ChapterId,
    /// Location to save the pages.
    #[structopt(parse(from_os_str), default_value = "./")]
    output: PathBuf,
}

#[tokio::main]
async fn main() {
    let opt = Opt::from_args();

    if let Err(e) = run(opt).await {
        use std::process;
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}

async fn run(opt: Opt) -> Result<(), Error> {
    let mangadex_client = MangaDexV2::default();

    if !opt.output.is_dir() {
        let _ = create_dir(&opt.output);
    }

    println!("Downloading cover images into \"{}\"", opt.output.display());

    let chapter = mangadex_client
        .chapter()
        .chapter_id(opt.chapter_id)
        .send()
        .await
        .unwrap()
        .ok()
        .unwrap();

    for page in chapter.data().pages() {
        let page_url = format!(
            "{server}/{chapter_hash}/{page}",
            server = chapter.data().server().trim_end_matches('/'),
            chapter_hash = chapter.data().hash(),
            page = page
        );

        println!("Downloading \"{}\"", parse_file_name_from_url(&page_url));
        download_file(mangadex_client.client(), &page_url, &opt.output).await?;
    }

    println!("Done");

    Ok(())
}

/// Parse the filename from the URL.
///
/// This does not have any safety guarantees so if there is an error, it will panic.
fn parse_file_name_from_url<U: Into<String>>(url: U) -> String {
    Path::new(url.into().as_str())
        .file_name()
        .unwrap()
        .to_str()
        .unwrap()
        .split('?')
        .next()
        .unwrap()
        .into()
}

/// Download the URL contents into the local filesystem.
async fn download_file<U: Into<String>>(
    client: &Client,
    url: U,
    output: &PathBuf,
) -> Result<(), Error> {
    let url = url.into();
    let file_name = parse_file_name_from_url(url.clone());

    let bytes = client.get(url.as_str()).send().await?.bytes().await?;
    let mut file_buffer = File::create(output.join(file_name)).unwrap();
    let _ = file_buffer.write_all(&bytes);

    Ok(())
}
