# Table of Contents

* [Download chapter](#download-chapter)
* [Download manga covers](#download-manga-covers)
* [Login](#login)
* [v2 CLI Example](#v2-cli-example)
    * [Fetch detailed chapter information](#fetch-detailed-chapter-information)
    * [Fetch all follow types](#fetch-all-follow-types)
    * [Fetch detailed group information](#fetch-detailed-group-information)
    * [Fetch detailed manga information](#fetch-detailed-manga-information)
    * [Fetch partial information about chapters belonging to a manga](#fetch-partial-information-about-chapters-belonging-to-a-manga)
    * [Fetch a list of manga covers](#fetch-a-list-of-manga-covers)
    * [Fetch all manga relation types](#fetch-all-manga-relation-types)
    * [Fetch all tags](#fetch-all-tags)
    * [Fetch detailed tag information](#fetch-detailed-tag-information)
    * [Fetch partial information about chapters uploaded by a user](#fetch-partial-information-about-chapters-uploaded-by-a-user)
    * [Fetch a user's followed manga](#fetch-a-users-followed-manga)
    * [Fetch a user's followed manga updates](#fetch-a-users-followed-manga-updates)
    * [Fetch a user's personal data for a manga](#fetch-a-users-personal-data-for-a-manga)
    * [Fetch a user's manga ratings](#fetch-a-users-manga-ratings)
    * [Mark a chapter as read or unread for a user](#mark-a-chapter-as-read-or-unread-for-a-user)
    * [Fetch a user's settings on MangaDex](#fetch-a-users-settings-on-mangadex)
    * [Fetch the latest manga updates](#fetch-the-latest-manga-updates)
    * [Fetch the most popular manga](#fetch-the-most-popular-manga)
    * [Search for manga](#search-for-manga)

# Download chapter

Download the pages of a chapter.

If the output directory does not exist, it will be created. However, if the parent of the given
path doesn't exist, a panic will be thrown.

## Usage

```
cargo run --example download_chapter -- <chapter_id> [output]
```

* `chapter_id` - Numerical chapter ID.
* `output` - The location to save the cover image(s). Default: `./`

# Download manga covers

Download the cover(s) of the specified manga ID.

If the output directory does not exist, it will be created. However, if the parent of the given
path doesn't exist, a panic will be thrown.

## Usage

```
cargo run --example download_manga_covers -- <manga_id> [output] [--all]
```

* `manga_id` - Numerical manga ID.
* `output` - The location to save the cover image(s). Default: `./`.
* `--all` - Include this flag to download all the manga covers, not just the main cover.

# Login

Prompts you to enter your username, password, and 2FA code if needed. If all is successful, then
a success message is displayed, and it exits.

Nothing is stored in this example; it just shows a working example of logging in.

## Usage

```
cargo run --example login
```

## Sample Output

```
$ cargo run --example login
Enter your MangaDex username:
my_username
Enter your MangaDex password:
my_password
Enter your 2FA code:
123456
Login successful!
```

# v2 CLI Example

[Back to top](#table-of-contents)

Print the results of simple HTTP GET requests to the MangaDex unrestricted APIs using the
internal struct representation.

## Fetch detailed chapter information

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/chapter/{chapterId|hash}` endpoint.

### Usage

```
cargo run --example v2_cli -- --chapter [chapter_id]
```

## Fetch all follow types

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/follows` endpoint.

### Usage

```
cargo run --example v2_cli -- --follow-types
```

## Fetch detailed group information

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/group/{groupId}` endpoint.

### Usage

```
cargo run --example v2_cli -- --group [group_id]
```

## Fetch detailed manga information

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/manga/{mangaId}` endpoint.

### Usage

```
cargo run --example v2_cli -- --manga [manga_id]
```

## Fetch partial information about chapters belonging to a manga

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/manga/{mangaId}/chapters` endpoint.

### Usage

```
cargo run --example v2_cli -- --manga-chapters [manga_id] [--page [page_num] --limit [return_limit]]
```

## Fetch a list of manga covers

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/manga/{mangaId}/covers` endpoint.

### Usage

```
cargo run --example v2_cli -- --manga-covers [manga_id]
```

## Fetch all manga relation types

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/relations` endpoint.

### Usage

```
cargo run --example v2_cli -- --relation-types
```

## Fetch all tags

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/tag` endpoint.

### Usage

```
cargo run --example v2_cli -- --tags
```

## Fetch detailed tag information

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/tag/{tagId}` endpoint.

### Usage

```
cargo run --example v2_cli -- --tag [tag_id]
```

## Fetch detailed user information

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}` endpoint.

### Usage

```
cargo run --example v2_cli -- --user [user_id|me]
```

## Fetch partial information about chapters uploaded by a user

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}/chapters` endpoint.

### Usage

```
cargo run --example v2_cli -- --user-chapters [user_id|me] [--page [page_num] --limit [return_limit]]
```

## Fetch a user's followed manga

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}/followed-manga` endpoint.

### Usage

```
cargo run --example v2_cli -- --user-followed-manga [user_id|me]
```

## Fetch a user's followed manga updates

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}/followed-updates` endpoint.

You must first log in before accessing this feature.

### Usage

```
cargo run --example v2_cli -- --username "my_username" --password "my_password" --user-followed-updates [user_id|me]
```

## Fetch a user's personal data for a manga

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}/manga/{mangaId}` endpoint.

You must first log in before accessing this feature.

### Usage

```
cargo run --example v2_cli -- --username "my_username" --password "my_password" --user-manga-data [user_id|me] [manga_id]
```

## Fetch a user's manga ratings

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}/ratings` endpoint.

You must first log in before accessing this feature.

### Usage

```
cargo run --example v2_cli -- --username "my_username" --password "my_password" --user-manga-ratings [user_id|me]
```

## Mark a chapter as read or unread for a user

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}/marker/` endpoint.

You must first log in before accessing this feature.

### Usage

```
cargo run --example v2_cli -- --username "my_username" --password "my_password" --user-chapters-marker [user_id|me] [chapter_id]
```

## Fetch a user's settings on MangaDex

[Back to top](#table-of-contents)

This will print the result of the `/api/v2/user/{userId|me}/settings/` endpoint.

You must first log in before accessing this feature.

### Usage

```
cargo run --example v2_cli -- --username "my_username" --password "my_password" --user-settings [user_id|me]
```

## Fetch the latest manga updates

[Back to top](#table-of-contents)

This will scrape and print partial manga information from the `/updates/{page}` page.

### Usage

```
cargo run --example v2_cli -- --latest-updates [--page <page>] [--languages <flags>]
```

* `<flags>` - Two-letter lowercase country code (Follows the [ISO 3166 Alpha-2 standard][ISO 3166]).

## Fetch the most popular manga

[Back to top](#table-of-contents)

This will scrape and print partial manga information from the `/titles/7/{page}` page.

"7" is the sort-by ID to sort by rating in descending order.

### Usage

```
cargo run --example v2_cli -- --most-popular [--page <page>]
```

## Search for manga

[Back to top](#table-of-contents)

This will scrape and print the partial manga information from the `/search` page.

You must first log in before accessing this feature.

### Usage

```
cargo run --example v2_cli -- --username "my_username" --password "my_password" --search-title <title>
```

[ISO 3166]: https://www.iso.org/iso-3166-country-codes.html
