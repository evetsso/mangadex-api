# Version 0.2.2 (2021-01-08)

## Dependencies

- [Update Reqwest to 0.11][b8e82c6]
- [Update Tokio to 1.0][b8e82c6]

[b8e82c6]: https://gitlab.com/gondolyr/mangadex-api/-/commit/b8e82c6d414717dabfa8c6650f6bf52f16c4dc99

# Version 0.2.0 (2020-12-29)

The web scraping search, fetch latest updates, and most popular manga functionality
all return a struct with the manga ID, title, and cover image URL.

## Added

- [Added `SiteTheme` enum for choosing the website theme][fc45db0]
- [Added functionality to fetch the latest manga updates via web scraping][bcfd464]
- [Added search functionality via web scraping][84216cb]
- [Added functionality to fetch the most popular manga (highest Bayesian rating) via web scraping][2d616df]
- [Added a `builder()` method to the `MangaDexV2` struct to allow for API configuration for things such as the user agent][f5874bd]
- [Added `tags()` method to `TagCategory` enum to return the associated tags with the category][0375fe6]

## Changed

- \[BREAKING\] [Changed `Language` default value to `English` because MangaDex appears to default to this language][9804cab]
- \[BREAKING\] [Make "time"/"Chrono" feature on by default to make it easier to work with dates and times][66845e9]
- [Changed enum function arguments to accept generic values for improved flexibility][8f296ef]
- [Make `Language` enum `From` trait implementations case-insensitive][4b522b9]

## Examples

- [Added login example to README][2705240]

## Fixed

- [Fixed `Language::Other` integer value][9804cab]

## Internal Only

- [Implement `FromStr` trait on Language enum][2c4d698]
- [Removed patch versions from Cargo.toml because the latest patch fixes should not include any breaking
  changes and should instead bring the latest fixes][2891a55]

[2c4d698]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2c4d69860f729579bcffbfaacdd11c3bf0d79421
[fc45db0]: https://gitlab.com/gondolyr/mangadex-api/-/commit/fc45db08c500cec8a665351a4df3dd90e80f664a
[9804cab]: https://gitlab.com/gondolyr/mangadex-api/-/commit/9804cab889284de1a463bedf8fd6e33fa945930d
[bcfd464]: https://gitlab.com/gondolyr/mangadex-api/-/commit/bcfd4641c9ad74b88c7c2783fdb2af6e1776f4d6
[84216cb]: https://gitlab.com/gondolyr/mangadex-api/-/commit/84216cbe3a930bdf6db1f271c1dd0749d42a0b9a
[8f296ef]: https://gitlab.com/gondolyr/mangadex-api/-/commit/8f296efaa8a0583bf9722229de773bd8abfb468b
[66845e9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/66845e9dc3fffed4289353bcf7e68f002f65343b
[2d616df]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2d616dfdab870eacb9e1b348b2c6cf1da129c0b6
[f5874bd]: https://gitlab.com/gondolyr/mangadex-api/-/commit/f5874bd6e5fb826e99beec7cd56a89b86c917678
[4b522b9]: https://gitlab.com/gondolyr/mangadex-api/-/commit/4b522b9adf8d8609e8f64a3943a49db484db882d
[2891a55]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2891a55a1991916b25d1c7118fe6c038216fa0ec
[0375fe6]: https://gitlab.com/gondolyr/mangadex-api/-/commit/0375fe6fc46343ff4a07a38b1053c6aeb9d3819c
[2705240]: https://gitlab.com/gondolyr/mangadex-api/-/commit/27052400efdc3feecb6c3ce56c5505764cbf01d5

# Version 0.1.1 (2020-12-11)

## Improvements

- [Added `Copy` trait to internal MangaDex types.][ac533c0a]
- [Added `Hash` and `Serialize` to internal MangaDex types.][135f2c69]

## Documentation

- [Added GitLab pages job to publish Cargo-generated docs onto project website.][4d9065e1]
- [Fixed README example and v2 client docblocks return types.][f2197738]
- [Added Contributing guide.][21da4ca6]

## Examples

- [Added manga cover image downloader.][e9980599]
- [Added chapter downloader.][2f04b936]

## Internal Only

- [Moved `TagCategory` out of `tag` module to `tag_category`.][883b0e15]
- [Removed `getset` crate and implemented own getters.][da206ffc]

[4d9065e1]: https://gitlab.com/gondolyr/mangadex-api/-/commit/4d9065e180c87f1bd4be781171714b6d52e2f9ed
[f2197738]: https://gitlab.com/gondolyr/mangadex-api/-/commit/f219773828538783267eed7cf0534724735566c9
[21da4ca6]: https://gitlab.com/gondolyr/mangadex-api/-/commit/21da4ca6c12f282b086a835760e9616174242b67
[e9980599]: https://gitlab.com/gondolyr/mangadex-api/-/commit/e9980599c50073d621edc31e85cd6a6c89764788
[ac533c0a]: https://gitlab.com/gondolyr/mangadex-api/-/commit/ac533c0a05183d4e678387dd148551a4bc575fdf
[883b0e15]: https://gitlab.com/gondolyr/mangadex-api/-/commit/883b0e158bcc8835be075c10803c42362c5f3fe1
[135f2c69]: https://gitlab.com/gondolyr/mangadex-api/-/commit/135f2c69b39ea304ed0e29e3b0f301c668d08c6d
[2f04b936]: https://gitlab.com/gondolyr/mangadex-api/-/commit/2f04b93667390b28729d910759f206670819780b
[da206ffc]: https://gitlab.com/gondolyr/mangadex-api/-/commit/da206ffca29657173dd1f28e2a475468657fc9b2

# Version 0.1.0 (2020-12-06)

## Features

- [All (most) MangaDex v2 endpoints implemented as of 2020-12-06.][endpoints]
  - API index page is not implemented. At this time, there is no intention to implement this unless there is strong demand.
- [Can log in to access restricted endpoints.][d8f794f8]
- [Chrono is a feature that will transmute various date/time fields as Chrono types][9d71c29b]
- [Example CLI tool for calling all the implemented v2 endpoints.][6c1f024c]
    - This does not have a lot of customization as it is designed to demonstrate basic usage.
- [Many MangaDex types have been internalized to Rust enums and structs for ease-of-use.][types]

[9d71c29b]: https://gitlab.com/gondolyr/mangadex-api/-/commit/9d71c29b3ca41201fe89b3fd351cc8792a522522
[6c1f024c]: https://gitlab.com/gondolyr/mangadex-api/-/commit/6c1f024cde9cd396e0fd0b260b0f82c455eccd40
[d8f794f8]: https://gitlab.com/gondolyr/mangadex-api/-/commit/d8f794f860f8b93c306fd52e7ba61d013c88bc1f
[endpoints]: https://gitlab.com/gondolyr/mangadex-api/-/tree/0.1.0/src/v2/builder
[types]: https://gitlab.com/gondolyr/mangadex-api/-/tree/0.1.0/src/types
